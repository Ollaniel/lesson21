package by.train.java.xml.dom;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.List;

import org.junit.Test;
import org.xml.sax.SAXException;

import by.train.java.xml.bean.Good;

public class DomParseTest {
	List<Good> gd;
	
	
	@Test
	public void test() {
		try {
			DomParser p= new DomParser("d:\\work\\JAVA\\lesson21\\1.xml");
			gd=p.parse();
			for (Good gg :gd){
				System.out.println(gg);	
			}
			assertTrue(gd.size()==2);
		} catch (SAXException | IOException e) {
			fail("Exception fired. " + e.getMessage());
		}
	}

}
