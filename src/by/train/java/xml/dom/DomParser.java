package by.train.java.xml.dom;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import by.train.java.xml.bean.Good;

import javax.xml.parsers.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DomParser {
	private Document doc;
	private List<Good> gd = new ArrayList<Good>();
	List<String> setOfNames = new ArrayList<String>();// куда делся атрибут доступа?
	
	public DomParser (String flName) throws SAXException, IOException {
        File inputFile = new File(flName);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
	        doc = dBuilder.parse(inputFile);
		} catch (ParserConfigurationException e) {//как-будто исключеи я и не учили
			e.printStackTrace();// к чему приведет просто гашение исключения здесь?
		}
		String[] b ={"name","description","customer"};// зачем так сложно?
		setOfNames.addAll(Arrays.asList(b));
	}
	/***
	 * To parse xml that was set during parser creation.
	 * @return list of Goods
	 */
	public List<Good> parse () {
		gd = new ArrayList<Good>();
		Good g=null;
        doc.getDocumentElement().normalize();
        NodeList nList = doc.getElementsByTagName("good");
        for (int temp = 0; temp < nList.getLength(); temp++) {
           Node nNode = nList.item(temp);
           g=new Good();
           if (nNode.getNodeType() == Node.ELEMENT_NODE) {
              Element eElement = (Element) nNode;
              g.setId(Integer.parseInt(eElement.getAttribute("id")));
              for (String s:setOfNames) {
            	switch (s) {  
            		case "name": g.setName(eElement.getElementsByTagName(s).item(0).getTextContent());
            					break;
            		case "description": g.setDescr(eElement.getElementsByTagName(s).item(0).getTextContent());
								break;
            		case "customer":
            					NodeList elm=eElement.getElementsByTagName(s);
            					if (elm.getLength()>0 ) {
            						g.setCustId(Integer.parseInt(elm.item(0).getTextContent()));
            					}	
								break;
            	}
              }
           }
           gd.add(g);
        }
		return gd;
	}
	
}	

